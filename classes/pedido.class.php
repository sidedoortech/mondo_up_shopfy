<?php
class Pedido
{
	var $cod_cli;
	var $pedido = array();
	var $Importado = "";

	function __construct($api, $id_order, $cod_cliente, $produtos, $total, $frete)
	{
		// cod_cli = $cliente->listClienteAlteracaos[0]->coD_CLI;
		//$this->pedido["COD_PED_WEB"] = strval($id_order);
		if (!$cod_cliente) {
			return $this;
		}
		$this->pedido["COD_CLI"] = $cod_cliente;
		$this->pedido["COD_VENDEDOR"] = COD_VENDEDOR;
		$this->pedido["VALOR_PED"] = $total;
		$this->pedido["VALOR_FRETE"] = $frete;
		$this->pedido["Itens"] = $produtos->produto;

		/* Verifica a Tag de Pedido Integrado */
		$result = $api->rest('GET', '/admin/orders/' . $id_order . '.json');
		$this->Importado = $result["body"]["order"]["tags"];
		/* Verifica a Tag de Pedido Integrado */

		/* Adicionar Tag de Pedido Integrado */
		$result = $api->rest('PUT', '/admin/orders/' . $id_order . '.json', array("order" => array("id" => $id_order, "tags" => "Importado")));
		/* Adicionar Tag de Pedido Integrado */
	}

	function Incluir()
	{
		if ($this->Importado == "") { //verifica se o pedido já está integrado
			$requisicao = new Requisicao("Pedidos/cadastro", $this->pedido);
			$response = $requisicao->POST();
			return $response;
		} else {
			return "Já Importado";
		}
	}
}