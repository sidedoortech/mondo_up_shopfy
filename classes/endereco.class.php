<?php

use Jarouche\ViaCEP\HelperViaCep;

class Endereco
{
	var	 $enderecos = array();
	var  $telefone;
	function __construct($shopify)
	{
		function tirarAcentos($string)
		{

			return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/"), explode(" ", "a A e E i I o O u U n N"), iconv(mb_detect_encoding($string, mb_detect_order(), true), "UTF-8", $string));
		}

		$cep1 = HelperViaCep::getBuscaViaCEP('JSON', $shopify->billing_address->zip);
		$this->enderecos[0]["APELIDO"] = tirarAcentos($shopify->billing_address->first_name . " " . $shopify->billing_address->last_name);
		$this->enderecos[0]["ENDERECO"] = tirarAcentos($shopify->billing_address->address1);
		$this->enderecos[0]["NUMERO"] = "-";
		$this->enderecos[0]["COMPLEMENTO"] = tirarAcentos($shopify->billing_address->address2);
		$this->enderecos[0]["BAIRRO"] = empty($cep1['result']['bairro']) ? 'SEM BAIRRO' : tirarAcentos($cep1['result']['bairro']);
		$this->enderecos[0]["CIDADE"] = tirarAcentos($shopify->billing_address->city);
		$this->enderecos[0]["CEP"] = tirarAcentos($shopify->billing_address->zip);
		$this->enderecos[0]["UF"] = tirarAcentos($shopify->billing_address->province_code);
		$this->enderecos[0]["TIPO"] = tirarAcentos("Cobrança");
		$this->telefone = $shopify->billing_address->phone;
		if (!isset($shopify->shipping_address)) {
			$this->enderecos[1]["APELIDO"] = tirarAcentos($shopify->billing_address->first_name . " " . $shopify->billing_address->last_name);
			$this->enderecos[1]["ENDERECO"] = tirarAcentos($shopify->billing_address->address1);
			$this->enderecos[1]["NUMERO"] = "-";
			$this->enderecos[1]["COMPLEMENTO"] = tirarAcentos($shopify->billing_address->address2);
			$this->enderecos[1]["BAIRRO"] = empty($cep1['result']['bairro']) ? 'SEM BAIRRO' : tirarAcentos($cep1['result']['bairro']);
			$this->enderecos[1]["CIDADE"] = tirarAcentos($shopify->billing_address->city);
			$this->enderecos[1]["CEP"] = preg_replace('/[^0-9]/', '', tirarAcentos($shopify->billing_address->zip));
			$this->enderecos[1]["UF"] = tirarAcentos($shopify->billing_address->province_code);
			$this->enderecos[1]["TIPO"] = tirarAcentos("Entrega");
		} else {
			$cep2 = HelperViaCep::getBuscaViaCEP('JSON', $shopify->shipping_address->zip);
			$this->enderecos[1]["APELIDO"] = tirarAcentos($shopify->shipping_address->first_name . " " . $shopify->shipping_address->last_name);
			$this->enderecos[1]["ENDERECO"] = tirarAcentos($shopify->shipping_address->address1);
			$this->enderecos[1]["NUMERO"] = "-";
			$this->enderecos[1]["COMPLEMENTO"] = tirarAcentos($shopify->shipping_address->address2);
			$this->enderecos[1]["BAIRRO"] = empty($cep2['result']['bairro']) ? 'SEM BAIRRO' : tirarAcentos($cep2['result']['bairro']);
			$this->enderecos[1]["CIDADE"] = tirarAcentos($shopify->shipping_address->city);
			$this->enderecos[1]["CEP"] = preg_replace('/[^0-9]/', '', tirarAcentos($shopify->shipping_address->zip));
			$this->enderecos[1]["UF"] = tirarAcentos($shopify->shipping_address->province_code);
			$this->enderecos[1]["TIPO"] = tirarAcentos("Entrega");
		}
	}

	function obterEnderecos()
	{
		return array("Endereco" => $this->enderecos, "Telefone" => $this->telefone);
	}
}