<?php

class Cliente
{
	var $dadosDoCliente = array();
	var $cliente = array();
	function __construct($api, $cliente, $enderecos, $order_id)
	{

		$this->cliente = $cliente;
		/* Resgatando o CPF do Cliente*/
		$result = $api->graph('{order(id: "gid://shopify/Order/' . $order_id . '"){ id localizationExtensions(first: 5){ edges { node { countryCode purpose title value } } } } }');

		$cpf = $result["body"]["container"]["data"]["order"]["localizationExtensions"]["edges"][0]["node"]["value"];

		/* Resgatando o CPF do Cliente*/

		$this->dadosDoCliente["CPF_CNPJ"] = $cpf; //https://community.shopify.com/c/Perguntas-t%C3%A9cnicas-frequentes/Importa%C3%A7%C3%A3o-do-CPF-para-eRP-novo-campo/td-p/879956#
		$this->dadosDoCliente["RazaoSocial"] = $cliente->first_name . " " . $cliente->last_name;
		$this->dadosDoCliente["NomeFantasia"] = $cliente->first_name . " " . $cliente->last_name;
		$this->dadosDoCliente["Celular"] = $enderecos["Telefone"]; // se tiver nulo, tentar achar nos endereços
		$this->dadosDoCliente["Telefones"] = $enderecos["Telefone"];
		$this->dadosDoCliente["Email"] = $cliente->email;
		$this->dadosDoCliente["Enderecos"] = $enderecos["Endereco"];
	}

	function dadosCliente()
	{
		return $this->dadosDoCliente;
	}

	function verificarExistenciaDeCliente()
	{
		$requisicao = new Requisicao("clientes/consulta/" . $this->dadosDoCliente["CPF_CNPJ"]);
		return $requisicao->GET();
	}

	function Incluir()
	{
		$BuscaClientePorCPF = $this->verificarExistenciaDeCliente();


		if (count($BuscaClientePorCPF) == 0) {
			$requisicao = new Requisicao("clientes/cad", $this->dadosDoCliente);
			$response = $requisicao->POST();

			return $response;
		} else {
			foreach ($BuscaClientePorCPF as $cli) {
				$requisicao = new Requisicao("clientes/alt/" . $cli->codCli, $this->dadosDoCliente);
				$response = $requisicao->PUT();


				return $response;
			}
		}
	}
}