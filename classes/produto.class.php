<?php
class Produto
{
	var $produto = array();
	function __construct($produto)
	{
		$i = 0;

		foreach ($produto as $pro) {
			if (in_array($pro->sku, $this->produtosNoERP())) {
				$this->produto[$i]["COD_PROD"] = $pro->sku;
				$this->produto[$i]["QTD"] = $pro->quantity;
				$this->produto[$i]["VR_UNIT"] = (isset($pro->discount_allocations[0]->amount)) ? ($pro->price - ($pro->discount_allocations[0]->amount / $pro->quantity)) : $pro->price;
				$this->produto[$i]["UNID"] = 'CX';
				$this->produto[$i]["PESO_BRUTO"] = ($pro->grams / 1000);
				$i++;
			}
		}
	}

	function obterProdutos()
	{
		return $this->produto;
	}

	function produtosNoERP()
	{
		$requisicao = new Requisicao("produtos/consulta/todos");
		$produtos = $requisicao->GET();

		$proA = array();

		foreach ($produtos as $p) {
			$proA[] = $p->coD_PROD;
		}

		return $proA;
	}
}