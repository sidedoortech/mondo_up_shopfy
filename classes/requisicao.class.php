<?php
Class Requisicao{
	var $endpoint = "http://upapi.cloudupsoftware.com/UpApi/v1/API/";
	var $url; 
	var $cabecalho = array('Content-Type'=>'application/json');
	var $params;
	var $token; 
	function __construct($url,$params = []){
		$this->url = $url;
		$this->params = json_encode($params);
	}
		
	function token(){
		$response = \Httpful\Request::post($this->endpoint."Token")->addHeaders($this->cabecalho)->body(json_encode(array("ID_Cliente"=>ID_CLIENTE,"homolog"=>HOMOLOGACAO)))->send();					
		return json_decode($response->body)->token;
	}
	
	function POST(){
		$this->cabecalho["Authorization"] = "Bearer ".$this->token();
		$response = \Httpful\Request::post($this->endpoint.$this->url)->addHeaders($this->cabecalho)->body($this->params)->send();					
		return json_decode($response->body);
	}
	
	function GET(){
		$this->cabecalho["Authorization"] = "Bearer ".$this->token();
		$response = \Httpful\Request::get($this->endpoint.$this->url)->addHeaders($this->cabecalho)->send();	
		return json_decode($response->body);
	}
	
	function PUT(){
		$this->cabecalho["Authorization"] = "Bearer ".$this->token();
		$response = \Httpful\Request::put($this->endpoint.$this->url)->addHeaders($this->cabecalho)->body($this->params)->send();	
		return json_decode($response->body);
	}
}