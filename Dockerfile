FROM php:7.1-fpm
RUN apt-get update && apt-get install -y libmcrypt-dev && docker-php-ext-install mcrypt pdo_mysql
ADD . /var/www
RUN chown -R www-data:www-data /var/www

# Set permissions
ARG PUID=33
ARG PGID=33
RUN groupmod -g $PGID www-data \
  && usermod -u $PUID www-data

RUN chown -R www-data:www-data /var/www
RUN chmod 755 /var/www