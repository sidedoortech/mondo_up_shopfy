<?php

header("content-type:text/plain");



require "classes/cliente.class.php";
require "classes/produto.class.php";
require "classes/endereco.class.php";
require "classes/pedido.class.php";
require "classes/requisicao.class.php";
require "bootstrap.php";
require "vendor/autoload.php";

const COD_VENDEDOR   = "5"; // Adicionar código do vendedor do ERP UP
const HOMOLOGACAO    = "false"; // Adicionar Ambiente do ERP UP
const ID_CLIENTE     = "U>B?EGA@?"; // Adicionar código cliente do ERP UP
const ENDERECO_LOJA  = "mondo-vegano.myshopify.com";
const TOKEN_SHOPIFY  = "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";
const VERSAO_SHOPIFY = "2020-10";
//Recebendo o Webhook
@$body = file_get_contents("php://input");

$shopify = json_decode($body);

$dt = date('Y-m-d-H-i-s');

$dir = "logs/" . $shopify->id;
if (is_dir($dir) === false) {
	mkdir($dir, 0755, true);
}


if ($shopify->financial_status == "paid") {

	$myfile = fopen($dir . "/" . $dt . "recebido_shopfy.json", "wb") or die("Unable to open file!"); //gerar um arquivo de log para analise, usado para debug
	$txt = $body;
	fwrite($myfile, $txt); // Usado para debug do recebimento do webhook


	$options = new Osiset\BasicShopifyAPI\Options();
	$options->setVersion(VERSAO_SHOPIFY);
	$api = new Osiset\BasicShopifyAPI\BasicShopifyAPI($options);
	$api->setSession(new Osiset\BasicShopifyAPI\Session(ENDERECO_LOJA, TOKEN_SHOPIFY));

	// $result = $api->graph('{order(id: "gid://shopify/Order/3082597793977' . $shopify->id . '"){ id localizationExtensions(first: 5){ edges { node { countryCode purpose title value } } } } }');
	// $cpf = $result["body"]["container"]["data"]["order"]["localizationExtensions"]["edges"][0]["node"]["value"];
	// $myfile = fopen("logs/retorno_pedido_" . $cpf . ".txt", "wb") or die("Unable to open file!");
	// $txt = print_r($result, true); //gerar um arquivo de log para analise, usado para debug
	// fwrite($myfile, $txt);


	//Enderecos
	$endereco = new Endereco($shopify);

	// $class_cep = HelperViaCep::getBuscaViaCEP('JSON', '01311300');
	// var_dump($class_cep['result']);

	// var_dump($endereco);

	//Cliente
	$customer =  $shopify->customer;
	$cliente = new Cliente($api, $customer, $endereco->obterEnderecos(), $shopify->id);

	$myfile = fopen($dir . "/" . $dt . "inclusao_cliente.json", "wb") or die("Unable to open file!"); //gerar um arquivo de log para analise, usado para debug
	$txt = utf8_encode(print_r(json_encode($cliente), true));
	fwrite($myfile, $txt);

	// aqui tem que duplicar a chamada pq na primeira não vem o array de listClienteAlteracaos
	$cliente = $cliente->Incluir();


	$cod_cli = 0;
	if (empty($cliente->coD_CLI)) {
		$cod_cli = $cliente->listClienteAlteracaos[0]->coD_CLI;
	} else {
		$cod_cli = $cliente[0]->coD_CLI;
	}
	// print_r(json_encode($cliente->listClienteAlteracaos[0]->coD_CLI));

	$myfile = fopen($dir . "/" . $dt . "resposta_cliente.json", "wb") or die("Unable to open file!"); //gerar um arquivo de log para analise, usado para debug
	$txt = print_r(json_encode($cliente), true);
	fwrite($myfile, $txt);
	sleep(10);
	//Produto
	$produtos = new Produto($shopify->line_items);
	//Pedido	
	$pedidos = new Pedido($api, $shopify->id, $cod_cli, $produtos, $shopify->subtotal_price, $shopify->total_shipping_price_set->shop_money->amount);

	$myfile = fopen($dir . "/" . $dt . "pedido_montado.json", "wb") or die("Unable to open file!"); //gerar um arquivo de log para analise, usado para debug
	$txt = print_r(json_encode($pedidos), true);
	fwrite($myfile, $txt);


	$pedidoCriado = $pedidos->Incluir();


	$myfile = fopen($dir . "/" . $dt . "resposta_pedido.json", "wb") or die("Unable to open file!"); //gerar um arquivo de log para analise, usado para debug
	$txt = print_r(json_encode($pedidoCriado), true);
	fwrite($myfile, $txt);


	if (is_array($pedidoCriado)) {
		foreach ($pedidoCriado as $pc) {
			/* Adiciona uma nota com o número do pedido do ERP UP no Shopify */
			$result = $api->rest('PUT', '/admin/orders/' . $shopify->id . '.json', array("order" => array("id" => $shopify->id, "note" => "Pedido criado no ERP com código " . $pc->coD_PED . ".")));
			//$result = $api->rest('PUT','/admin/orders/'.$shopify->id.'.json',array("order"=>array("id"=>$shopify->id,"note"=>$txt))); //retorno da inclusão, usado para debug
			/* Adiciona uma nota com o número do pedido do ERP UP no Shopify */
			http_response_code(200);
		}
		print_r(json_encode($pedidoCriado), true);
	} else {
		http_response_code(200);
	}
}