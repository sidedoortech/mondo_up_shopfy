# Integração Shopify ERP UP
> Os pedidos gerados no shopify são enviados via webhook para o ERP UP.

# Cadastro do Webhook na Shopify

O Webhook deve ser cadastrado na shopify pelo Menu de Admin da Shopify em Configurações > Notificações > Webhooks

A url usada para receber as notificações:
```sh
https://mondo-up.sidedoor.tech/index.php
```

![](webhook.jpg)

## Exemplo de Pedido

Quando um pedido entra no ERP UP, a integração envia o código do pedido gerado no ERP para o Shopify como uma nota, permitindo assim identificar o pedido correspondente em ambas as plataformas.
Uma Tag "Importado" é adicionado também ao pedido no Shopify, para controle da integração.

![](obsShopify.png)


## Configuração da Integração

Lembrar de configurar a integração para o ambiente desejado, por padrão está configurado para o ambiente homologação, para ativar a produção só mudar o campo para FALSE. Algo importante é lembrar de pegar o código do Vendedor dentro do ERP, quando for passar para o ambiente Produção. 

```sh
const COD_VENDEDOR   = "57"; // Adicionar código do vendedor do ERP UP
const HOMOLOGACAO    = "TRUE"; // Adicionar Ambiente do ERP UP
const ID_CLIENTE     = "U>B?EGA@?";// Adicionar código cliente do ERP UP
const ENDERECO_LOJA  = "mondo-vegano.myshopify.com";
const TOKEN_SHOPIFY  = "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";
const VERSAO_SHOPIFY = "2020-10";
```

## Configuração de Produtos

Para integrar os produtos do ERP Up aos produtos da Shopify é necessário copiar o código do produto dentro do ERP Up e adiciona-lo no produto correspondente na shopify, usando o campo SKU:

![](skuShopify.png)
